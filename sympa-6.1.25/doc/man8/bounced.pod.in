=encoding utf8

=head1 NAME

bounced, bounced.pl - Mailing List Bounce Processing Daemon for WWSympa

=head1 SYNOPSIS

S<B<bounced.pl> [ B<-F> ] [ B<-d, -dF> ]>

=head1 DESCRIPTION

Bounced is a program which scans permanently the bounce spool and
processes bounces (non-delivery messages), looking or bad addresses.
Bouncing addresses are tagged in database ; last bounce is kept for
each bvouncing address.

List owners will latter access bounces information via WWSympa.

=head1 OPTIONS

These programs follow the usual GNU command line syntax,
with long options starting with two dashes (`-').  A summary of
options is included below.

=over 5

=item B<-F>

Do not detach TTY.

=item B<-d>, B<-dF>

Run the program in a debug mode.

=back

=head1 FILES

F<--WWSCONFIG--> WWSympa configuration file.

F<--libexecdir--/bouncequeue> bounce spooler, referenced from sendmail alias file

F<--spooldir--/bounce> incoming bounces directory

F<--piddir--/bounced.pid> this file contains the process ID
of F<bounced.pl>.

=head1 MORE DOCUMENTATION

The full documentation can be
found in L<http://www.sympa.org/manual/>.

The mailing lists (with web archives) can be accessed at
L<http://listes.renater.fr/sympa/lists/informatique/sympa>.

=head1 AUTHORS

=over 4

=item Serge Aumont

ComitE<233> RE<233>seau des UniversitE<233>s

=item Olivier SalaE<252>n

ComitE<233> RE<233>seau des UniversitE<233>s

=back

Contact authors at <sympa-authors@listes.renater.fr>

This manual page was initially written by JE<233>rE<244>me Marant <jerome.marant@IDEALX.org>
for the Debian GNU/Linux system.

=head1 COPYRIGHT

Copyright E<169> 1997,1998,1999,2000,2001 ComitE<233> RE<233>seau des UniversitE<233>s

Copyright E<169> 1997,1998,1999 Institut Pasteur & Christophe Wolfhugel

You may distribute this software under the terms of the GNU General
Public License Version 2 (L<http://www.gnu.org/copyleft/gpl.html>)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.1 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts and no Back-Cover Texts.  A
copy of the license can be found under
L<http://www.gnu.org/licenses/fdl.html>.

=head1 BUGS

Report bugs to Sympa bug tracker.
See L<http://www.sympa.org/tracking>.

=head1 SEE ALSO

L<sympa(8)>, L<archived(8)>, L<mhonarc(1)>, L<wwsympa.conf(5)>.



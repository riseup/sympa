# Sympa::Constants.pm - This module contains all installation-related variables
# RCS Identication ; $Revision: 5768 $ ; $Date: 2009-05-21 16:23:23 +0200 (jeu. 21 mai 2009) $ 
#
# Sympa - SYsteme de Multi-Postage Automatique
#
# Copyright (c) 1997, 1998, 1999 Institut Pasteur & Christophe Wolfhugel
# Copyright (c) 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005,
# 2006, 2007, 2008, 2009, 2010, 2011 Comite Reseau des Universites
# Copyright (c) 2011, 2012, 2013, 2014 GIP RENATER
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Sympa::Constants;
use strict;

use Exporter;
our @ISA = qw(Exporter);


use constant VERSION => '6.1.25';
use constant USER    => 'sympa';
use constant GROUP   => 'sympa';

use constant CONFIG           => '/usr/local/sympa-dv/etc/sympa.conf';
use constant WWSCONFIG        => '/usr/local/sympa-dv/etc/wwsympa.conf';
use constant SENDMAIL_ALIASES => '/etc/mail/aliases-sympa-dv';

use constant PIDDIR     => '/usr/local/sympa-dv';
use constant EXPLDIR    => '/usr/local/sympa-dv/list_data';
use constant SPOOLDIR   => '/usr/local/sympa-dv/spool';
use constant SYSCONFDIR => '/usr/local/sympa-dv/etc';
use constant LOCALEDIR  => '/usr/local/sympa-dv/locale';
use constant LIBEXECDIR => '/usr/local/sympa-dv/bin';
use constant SBINDIR    => '/usr/local/sympa-dv/bin';
use constant SCRIPTDIR  => '/usr/local/sympa-dv/bin';
use constant MODULEDIR  => '/usr/local/sympa-dv/bin';
use constant DEFAULTDIR => '/usr/local/sympa-dv/default';
use constant STATICDIR  => '/usr/local/sympa-dv/static_content';
use constant ARCDIR    => '/usr/local/sympa-dv/arc';
use constant BOUNCEDIR  => '/usr/local/sympa-dv/bounce';

1;
